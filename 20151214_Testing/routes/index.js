var express = require('express'),
	auth = require('../lib/middleware').auth,
	home = require('./home'),
	library = require('./library'),
	bookInfo = require('./bookInfo');

module.exports.configure = function(app) {

	var router = express.Router();

	//log all requests
	router.use(function(req, res, next) {
		console.log(req.method + ' ' + req.url);
		next();
	});

	router.get('/', home.home);

	// shows the complete library
	router.get('/library', library.getLibrary);
	router.post('/addBook', library.addBook);

	//show the information of a book
	router.get('/bookInfo/:id/:user', auth.checkAuth, bookInfo.bookInfo);

	// mount the router on the app
	app.use('/', router);
};
