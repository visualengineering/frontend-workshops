'use strict';

var mongoDB = require("mongodb"),
	mongo = mongoDB.MongoClient,
	ObjectID = mongoDB.ObjectID,
	connectionString = "mongodb://localhost:27017/test";

module.exports.bookInfo = function(req, res) {
	console.log('/bookInfo');
	
	mongo.connect(connectionString, function(err, db){
		if (!err) { //We are connected

			var collection = db.collection('books');
			var book = collection.findOne({_id: ObjectID(req.params.id)},function(err,book){
				
				res.render('bookInfo', {
					title: book.title,
					author: book.author,
					year: book.year
				});
			});
			
		}
	});
};