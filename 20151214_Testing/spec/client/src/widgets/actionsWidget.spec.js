define(function(require) {
    var $ = require('jquery'),
        specUtils = require('specUtils'),
        ActionsWidget = require('widgets/actionsWidget'),
        pubsub = require('pubsub'),
        Book = require('models/book');

    describe("Actions Widget", function() {
        beforeEach(function(done) {
            specUtils.loadHtml('html/actions.html', done);
        });

        afterEach(function() {
            specUtils.removeHtml();
        });

        it("Should have a defined constructor", function() {
            expect(ActionsWidget).toBeDefined();
            expect(typeof ActionsWidget).toBe('function');
        });

        it("Should throw error if the constructor is called without passing a DOM associated element", function() {
            expect(function() { new ActionsWidget(); }).toThrow();
        });

        it("Should be able to create a new instance", function() {
            var mainEl = document.getElementById('actions'),
                instance = new ActionsWidget(mainEl);

            expect(instance).toBeDefined();
            expect(instance.mainEl).toBe(mainEl);
        });

        it("Should publish 'newBookAdded' event when a book is added", function() {
            var mainEl = document.getElementById('actions'),
                form = document.querySelectorAll('form')[0],
                instance = new ActionsWidget(mainEl),
                book = new Book('Jorge Luis Borges', '1944', 'Ficciones');
            
            spyOn(pubsub, 'publish');

            // Set up and submit the form.
            form.elements.author.value = book.author;
            form.elements.year.value = book.year;
            form.elements.title.value = book.title;
            form.querySelectorAll('button')[0].click();

            expect(pubsub.publish.calls.count()).toBe(1);
            expect(pubsub.publish).toHaveBeenCalledWith('newBookAdded', book);
        });

        it("Should publish 'disconnect' event when the disconnect button is clicked", function() {
            var mainEl = document.getElementById('actions'),
                instance = new ActionsWidget(mainEl);

            spyOn(pubsub, 'publish');

            document.getElementById('disconnect').click();
            expect(pubsub.publish.calls.count()).toBe(1);
            expect(pubsub.publish).toHaveBeenCalledWith('disconnect');
        });
    });
});