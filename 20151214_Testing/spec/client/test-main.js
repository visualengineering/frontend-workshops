(function(window, undefined) {
	var allTestFiles = [];

	// Get a list of all the test files to include
	for (var file in window.__karma__.files) {
		if (window.__karma__.files.hasOwnProperty(file) && /spec\.js$/.test(file)) {
			allTestFiles.push(file);
		}
	}

	require.config({
		// Karma serves files under /base, which is the basePath from your config file
		baseUrl: '/base/public/js',

		// example of using a couple path translations (paths), to allow us to refer to different library dependencies, without using relative paths
		paths: {
			'jquery': '../vendor/jquery/dist/jquery',
			'pubsub': 'lib/pubsub',
			'specUtils': '../../spec/client/support/utils'
		},

		// dynamically load all test files
		deps: allTestFiles,

		// we have to kickoff jasmine, as it is asynchronous
		callback: window.__karma__.start
	});

})(window);