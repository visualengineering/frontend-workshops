/*
'use strict';
	Tests server
*/
var nock = require('nock'),
	expect = require('chai').expect,
	sinon = require('sinon'),
	libraryRouter = require('../../../../routes/library'),
	mediator = require('../../../../routes/mediator');

var routeHandleGetLibrary = libraryRouter.getLibrary,
	req;

describe('Router library', function() {

	beforeEach(function(done) {
		// Configuramos las llamadas necesarias

		//Se reescribe la funcion getLibrary
		sinon.stub(mediator, 'getLibrary', function(callback){
			callback(null, {books: [{'name':'libro'}]});
		});
		// Initialize stores service
		done();
	});
	afterEach(function() {
		//restore wrapped
		mediator.getLibrary.restore();
	});

	it('Get library', function(done) {
        var res = {
            render: function(view, data){
				expect(view).to.equals('library');
				expect(data.books).to.be.an('array');
				done();
            }
        };

		// Ejecutamos la ruta
		routeHandleGetLibrary(req, res, done);
	});

	it('Add book', function (done) {
		//TODO
		done();
	});
});
