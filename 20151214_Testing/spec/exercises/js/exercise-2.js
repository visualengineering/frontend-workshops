(function() {
	'use strict';

	define(function(require) {
		var $ = require('jquery');

		var header;

		/**
		 * Retrieves the value of the private variable header.
		 */
		function getHeader() {
			return header;
		}

		/**
		 * Makes an ajax call sending some static data.
		 * 
		 * The json response is used to populate the <ul> with id 'list' with items information after a delay
		 * of 300ms.
		 * If an error occurs during the AJAX request, an error is shown to the user.
		 */
		function sendData() {
			$.ajax({
				url: 'send/static/data',
				method: 'POST',
				data: { 'data': 'static' },
				dataType: 'json',
				success: function(data, textStatus, jqXHR) {
					setTimeout(function() {
						var html = '';
						$.each(data.items, function() {
							html += '<li>' + this.text + ' (' + this.comment + ')' + '</li>';
						});
						$('#list').append(html);
					}, 300);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$('#error-message').text('Something went terribly wrong!');
				}
			});
		}

		/**
		 * Initializes the module.
		 *
		 * An ajax call is executed in order to populate the <ul> with id 'list' with html.
		 * If an error occurs during the AJAX request, an error is shown to the user.
		 */
		function init() {
			$.ajax({
				url: 'get/some/data',
				method: 'GET',
				dataType: 'html',
				success: function(data, textStatus, jqXHR) {
					if (jqXHR.getResponseHeader('x-header')) {
						header = jqXHR.getResponseHeader('x-header');
					}

					// Appends the html string returned by the server.
					$('#list').append(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					var $error = $('#error-message');

					if (jqXHR.status === 403) {
						// Invalid session. Show a special message to the user.
						$error.text('Your session has expired');
					} else {
						// Any other status show a generic error message.
						$error.text('Something went terribly wrong!');
					}
				}
			});
		}

		/**
		 * Resets the internal status of the module.
		 */
		function reset() {
			header = undefined;
		}

		return {
			init: init,
			sendData: sendData,
			getHeader: getHeader,
			reset: reset
		};
	});

}());



