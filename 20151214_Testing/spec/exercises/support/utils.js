/* global _dust, sinon, console */
(function() {
	'use strict';

	define(function(require) {
		var $ = require('jquery');

		/**
		 * Loads the fixture (html or json) located at 'path'.
		 * 
		 * @param  {String} path Absolute URI of the fixture to load.
		 * 
		 * @return {Object}      Returns the $.ajax promise object.
		 */
		function loadFixture(path) {
			return $.ajax({
				url: path,
				dataType: !!path.match(/\.json$/) ? 'json' : 'html'
			});
		}

		/**
		 * Loads the list of fixtures located at 'paths'.
		 * 
		 * @param {String/Array} fixtures: File paths for the fixtures to be loaded.
		 * @param {Function}     callback: Called once all the fixtures have been loaded. Receives
		 *                               an array of results with the loaded fixtures.
		 */
		function loadFixtures(paths, callback) {
			var results = [];

			if (!$.isArray(paths)) {
				paths = [paths];
			}

			$.when.apply($, $.map(paths, function(path) {
				return loadFixture('/base/spec/exercises/fixtures/' + path).then(function(data) {
					results.push(data);
				});
			})).done(function() {
				callback(results);
			});
		}

		/**
		 * Loads the list of html files in the test sandbox.
		 * 
		 * @param  {String/Array}   files    Paths for the html files.
		 * @param  {Function} callback Called once the html have been loaded.
		 */
		function loadHtml(files, callback) {
			var $testBox = $('#testSandbox');

			if ($testBox.length === 0) {
				$testBox = $('<div id="testSandbox"></div>').appendTo('body:first');
			}

			loadFixtures(files, function(fixtures) {
				$testBox.append( fixtures.join('') );
				callback();
			});
		}

		/**
		 * Removes the html contents of the test sandbox.
		 */
		function removeHtml() {
			$('#testSandbox').remove();
		}
		
		return {
			loadFixtures: loadFixtures,
			loadHtml: loadHtml,
			removeHtml: removeHtml
		};

	});
}());
