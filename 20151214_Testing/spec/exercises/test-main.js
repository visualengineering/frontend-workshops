(function(window, undefined) {
	var allTestFiles = [];

	// Get a list of all the test files to include
	for (var file in window.__karma__.files) {
		if (window.__karma__.files.hasOwnProperty(file) && /spec\.js$/.test(file)) {
			allTestFiles.push(file);
		}
	}

	require.config({
		// Karma serves files under /base, which is the basePath from your config file
		baseUrl: '/base/spec/',

		// example of using a couple path translations (paths), to allow us to refer to different library dependencies, without using relative paths
		paths: {
			// This path links the name 'jquery' to the jquery.js file in the path specified. Note that the path
			// is relative to the 'baseUrl' property of the requirejs configuration.
			'jquery': '../public/vendor/jquery/dist/jquery',

			// TODO: set up a path that links the name 'exercise-2' to the path where the source code for the exercise-2
			// is located (full path is '/spec/exercises/js/exercise-2.js). This name is used in the exercise-2.spec.js
			// file to load the module in order to test it.
			'': '',

			// TODO: set up a path that links the name 'specUtils' to the path where the source code for the support
			// utilities is located (full path is '/spec/exercises/support/utils.js'). This name is used in the spec
			// files to provide some useful functionalities.
			'': ''
		},

		// dynamically load all test files
		deps: allTestFiles,

		// we have to kickoff jasmine, as it is asynchronous
		callback: window.__karma__.start
	});

})(window);