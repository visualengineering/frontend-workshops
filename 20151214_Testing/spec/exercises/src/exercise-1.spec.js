define(function(require) {
    var specUtils = require('specUtils');

    describe("Exercise 1", function() {
        beforeEach(function(done) {
            // We load the HTMl in the exercise-1.html file into the DOM. You can take a look at this file
            // so that you understand better what we are trying in the spec below.
            specUtils.loadHtml('html/exercise-1.html', done);

            // TODO: Here you should define your custom matcher, so that it is available in the specs.
            // Remember that your custom matcher should be called 'toHaveChildren'.
            
        });

        afterEach(function() {
            specUtils.removeHtml();
        });

        it("Should be able to test if DOM elements have children with the 'toBe' or the custom matcher", function() {
            // TODO: Use the 'toBe' matcher with the element with id 'elist' to check that it has no childrens.

            // TODO: Use the 'toBe' matcher with the element with id 'flist' to check that it has has childrens.
             
            // TODO: Use the 'toHaveChildren' matcher with the element with id 'elist' to check that it has no childrens.

            // TODO: Use the 'toHaveChildren' matcher with the element with id 'flist' to check that it has childrens.
            
        });
    });
});