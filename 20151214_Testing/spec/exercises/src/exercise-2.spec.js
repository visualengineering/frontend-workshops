define(function(require) {
    var $ = require('jquery'),
        specUtils = require('specUtils'),
        exercise2 = require('exercise-2');

    describe("Exercise 2", function() {
        beforeEach(function(done) {
            specUtils.loadHtml('html/exercise-2.html', done);

            // TODO: Include here the Sinon fakeServer logic so that the AJAX calls in exercise-2.js are 
            // mocked.
            
        });

        afterEach(function() {
            specUtils.removeHtml();

            // TODO: Maybe you want to restore some configurations you may have done in the beforeEach function.
            
        });

        it("Should getData and replenish the list with the response of the server without setting header", function() {
            // TODO: Uncomment the call to the init method and test some expectation! Build new it's blocks until
            // you have 100% coverage of statements, branches and functions of the init method!
            
            // exercise2.init();
        });
    });
});