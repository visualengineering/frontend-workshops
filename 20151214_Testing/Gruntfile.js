module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		jshint: grunt.file.readJSON('jshint.json'),
		lint: {
			frontend: ['public/js/**/*.js']
		},
		watch: {
			files: ['<%= lint.frontend %>'],
			tasks: 'default'
		},
		requirejs: {
			build: {
				configFile: 'tools/requirejs/build.json'
			}
		},
		compileTemplates: {
			development: {
				templateFile: 'templates/index',
				destFile: 'index.html',
				requireScript: 'vendor/requirejs/require.js'
			},
			production: {
				templateFile: 'templates/index',
				destFile: 'build/index.html',
				requireScript: 'js/app.js'
			}
		},
		karma: {
			integration: {
				configFile: './spec/client/karma.conf.js'
			}
		},
		// Test server code
		simplemocha: {
			all: {
				src: 'spec/server/src/**/*.spec.js',
				options: {
					globals: ['describe', 'it'],
					timeout: 20000,
					slow: 50,
					ignoreLeaks: false,
					ui: 'bdd',
					reporter: 'spec'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-karma');
	grunt.loadNpmTasks('grunt-simple-mocha');

	grunt.loadTasks('./tools/grunt-tasks');

	grunt.registerTask('default', ['jshint:*']);
	grunt.registerTask('build', ['jshint:*','compileTemplates:development']);

	grunt.registerTask('production', ['jshint:*','compileTemplates:production','requirejs:build']);

	grunt.registerTask('test-client', ['karma:integration']);
	grunt.registerTask('test-server', ['simplemocha:all']);
	grunt.registerTask('test', ['test-server', 'test-client']);
};
