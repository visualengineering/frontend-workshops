var express = require('express'),
	cons = require('consolidate'),
	http = require('http'),
	path = require('path'),
	bodyParser = require('body-parser'),
	routes = require('../../routes');



module.exports.start = function(){
	var app = express();

	// Main configuration
	app.set('port', 3005);
	app.set('views', path.join(__dirname, '..', '..', 'templates'));
	app.set('view engine', 'dust');
	app.engine('dust', cons.dust);
	app.use(express.static(path.join(__dirname, '..', '..', 'public')));
	app.use(bodyParser.urlencoded());
	app.use(bodyParser.json());

	routes.configure(app);

	//error handler
	app.use(function(err, req, res, next) {
		if (req.xhr) {
    		res.status(500).send({ error: 'Something is wrong!' });
  		} else {
  			res.render('error', {
	  			message: err.stack
	  		});	
  		}
	});

	http.Server(app).listen(app.get('port'), function(){
		console.log('Express server listening on port ' + app.get('port'));
	});

};