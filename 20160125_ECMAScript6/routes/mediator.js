'use strict';

var mongoDB = require("mongodb"),
    mongo = mongoDB.MongoClient,
    ObjectID = mongoDB.ObjectID,
    connectionString = "mongodb://localhost:27017/test";

module.exports.getLibrary = function (callback) {
    mongo.connect(connectionString, function(err, db){
		var books = [],
			collection,
			stream;
		if (!err) { //We are connected

			collection = db.collection('books');
			stream = collection.find().stream();

			stream.on("data", function (book) {
				books.push(book);
			});

			stream.on("end", function () {
				//fired when there is no more document to look for
                callback(err, {books: books});
			});
		} else {
			callback(err);
		}
	});
};

module.exports.addBook = function (ctx, callback) {
    mongo.connect(connectionString, function(err, db){
		var collection,
			book;
		if (!err) {
			// We are connected
			collection = db.collection('books');
			book = ctx.req.body;
			book._id = ObjectID.str;

			collection.insert(book, function(err, result){
				if (err) {
					callback(err);
				} else {
                    callback(err, {id: book._id});
				}
			});

		} else {
			callback(err);
		}
	});
}
