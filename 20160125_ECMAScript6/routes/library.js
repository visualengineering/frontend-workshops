'use strict';

var mediator = require('./mediator');

module.exports.getLibrary = function(req, res, next) {
	// if you get stuck here, check your database is on!
	mediator.getLibrary(function(err, data){
		if (!err) {
			console.log('renderizamos la library');
			res.render('library', data);
		} else {
			next(err);
		}
	});
};

module.exports.addBook = function(req, res, next) {
	var ctx = {
		req : req,
		res : res
	};
	// if you get stuck here, check your database is on!
	mediator.addBook(ctx, function(err, data){
		if (!err) {
			res.json(data);
		} else {
			next(err);
		}
	});
};
