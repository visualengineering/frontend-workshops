/* global console */
(function(requirejs) {
	'use strict';

	requirejs.config({
		baseUrl: 'js/',
		paths: {
			jquery: '../vendor/jquery/dist/jquery',
			pubsub: 'lib/pubsub'
		}
	});
}(window.requirejs));





