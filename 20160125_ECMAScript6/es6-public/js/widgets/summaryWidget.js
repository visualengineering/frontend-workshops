export default class SummaryWidget {
	constructor(el) {
		this.mainEl = el;
	}

	render(books) {
		this.mainEl.innerHTML = `There are ${books.length} books in the library`;
	}
}
