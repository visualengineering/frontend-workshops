import * as pubsub from 'lib/pubsub';

export default class ListWidget {
	constructor(el) {
		this.mainEl = el;
	}

	render(books) {
		this.mainEl.innerHTML = '';
		
		books.forEach(book => {
			let el = document.createElement('li');
			
			el.innerHTML = book.bookInfo();
			this.mainEl.appendChild(el);
		});
	}
}
