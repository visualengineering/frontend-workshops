import * as pubsub from 'lib/pubsub';
import Book from 'models/book';

export default class ActionsWidget {
	constructor(el) {
		let formEl,
			disconnectEl;

		this.mainEl = el;

		formEl = this.mainEl.querySelectorAll('form')[0];
		disconnectEl = this.mainEl.querySelectorAll('#disconnect')[0];

		formEl.onsubmit = this.addBook;
		disconnectEl.onclick = this.disconnect;
	}

	addBook(ev) {
		let form,
			book;

		ev.preventDefault();
		form = ev.target;

		book = new Book(form.elements['author'].value, form.elements['year'].value, form.elements['title'].value);

		pubsub.publish('newBookAdded', book);

	}

	disconnect(ev) {
		ev.preventDefault();

		pubsub.publish('disconnect');
	}
}
