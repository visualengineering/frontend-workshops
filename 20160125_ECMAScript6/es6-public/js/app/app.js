import $ from 'jquery';
import * as pubsub from 'lib/pubsub';
import ActionsWidget from 'widgets/actionsWidget';
import ListWidget from 'widgets/listWidget';
import SummaryWidget from 'widgets/summaryWidget';

let subscriptionToken = null,
	booksCollection = [];

function addBook(book, done) {
	$.ajax({
		url: '/addBook',
		method: 'post',
		dataType: 'json',
		contentType: 'application/json',
		data: (typeof book === 'string') ? book : JSON.stringify(book),
		success: function(bookId) {
			book.setId(bookId);
			booksCollection.push(book);
			done();
		},
		error: function() {
			console.error('There was a problem adding your book');
		}
	});
}

function init(el, books) {
	let listEl = el.querySelectorAll('#list')[0],
		summaryEl = el.querySelectorAll('#summary')[0],
		actionsEl = el.querySelectorAll('#actions')[0],
		actionsWidget,
		listWidget,
		summaryWidget;

	books.forEach(book => {
		booksCollection.push(book);
	});

	actionsWidget = new ActionsWidget(actionsEl);
	listWidget = new ListWidget(listEl);
	summaryWidget = new SummaryWidget(summaryEl);

	subscriptionToken = pubsub.subscribe('newBookAdded', book => {
		addBook(book, function() {
			listWidget.render(booksCollection);
			summaryWidget.render(booksCollection);
		});
	});

	pubsub.subscribe('disconnect', function() {
		pubsub.unsubscribe(subscriptionToken);
	});
}

export { init as start };

