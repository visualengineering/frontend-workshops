export default class Book {
	constructor(author, year, title) {
		this.author = author;
		this.year = year;
		this.title = title;
 	}

	bookInfo() {
		return `${this.title}, written by ${this.author} on ${this.year}`;
	}

	setId(id) {
		this.id = id;
	}

	getId() {
		return this.id;
	}
}
