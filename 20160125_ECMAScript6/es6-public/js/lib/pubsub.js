let topics = {},
    subUid = -1;

export function publish(topic, args) {
    if ( !topics[topic] ) {
        return false;
    }
    
    let subscribers = topics[topic],
        len = subscribers ? subscribers.length : 0;
    
    while (len--) {
        subscribers[len].func(args);
    }
}

export function subscribe(topic, func) {
    if (!topics[topic]) {
        topics[topic] = [];
    }
    let token = (++subUid).toString();
    topics[topic].push({token: token, func: func});
    
    return token;
}

export function unsubscribe(token) {
    for (let m in topics) {
        if (topics[m]) {
            for (let i = 0, j = topics[m].length; i < j; i++) {
                if (topics[m][i].token === token) {
                    topics[m].splice(i, 1);
                    return token;
                }
            }
        }
    }
}