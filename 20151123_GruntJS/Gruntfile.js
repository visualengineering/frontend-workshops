module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		jshint: grunt.file.readJSON('jshint.json'),
		lint: {
			frontend: ['js/**/*.js']
		},
		watch: {
			files: ['<%= lint.frontend %>'],
			tasks: 'default'
		},
		requirejs: {
			build: {
				configFile: 'tools/requirejs/build.json'
			}
		},
		compileTemplates: {
			development: {
				templateFile: 'templates/index',
				destFile: 'index.html',
				requireScript: 'vendor/requirejs/require.js'
			},
			production: {
				templateFile: 'templates/index',
				destFile: 'build/index.html',
				requireScript: 'js/app.js'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.loadTasks('./tools/grunt-tasks');

	grunt.registerTask('default', ['jshint:*']);
	grunt.registerTask('build', ['jshint:*','compileTemplates:development']);

	grunt.registerTask('production', ['jshint:*','compileTemplates:production','requirejs:build']);
};