(function() {
    'use strict';
    
    define(function(require) {
        var topics = {},
            subUid = -1;
        function _publish(topic, args) {
            if ( !topics[topic] ) {
                return false;
            }
            var subscribers = topics[topic],
                len = subscribers ? subscribers.length : 0;
            while (len--) {
                subscribers[len].func(args);
            }
        }
        function _subscribe(topic, func) {
            if (!topics[topic]) {
                topics[topic] = [];
            }
            var token = (++subUid).toString();
            topics[topic].push({token: token, func: func});
            return token;
        }
        function _unsubscribe(token) {
            for (var m in topics) {
                if (topics[m]) {
                    for (var i = 0, j = topics[m].length; i < j; i++) {
                        if (topics[m][i].token === token) {
                            topics[m].splice(i, 1);
                            return token;
                        }
                    }
                }
            }
        }
        return {
            publish: _publish,
            subscribe: _subscribe,
            unsubscribe: _unsubscribe
        };
    });
}());