(function() {
	'use strict';

	define(function(require) {
		function Book( author, year, title ) {
			this.author = author;
			this.year = year;
			this.title = title;
		}

		Book.prototype.bookInfo = function() {
			return this.title + ", written by " + this.author + " on " + this.year;
		};

		Book.prototype.setId = function(id) {
			this.id = id;
		};

		Book.prototype.getId = function() {
			return this.id;
		};

		return Book;
	});
	
}());