(function(requirejs) {
	'use strict';

	requirejs.config({
		baseUrl: 'js/',
		paths: {
			jquery: '../vendor/jquery/dist/jquery',
			pubsub: 'lib/pubsub'
		}
	});

	define(function(require) {
		var $ = require('jquery'),
			pubsub = require('pubsub'),
			ActionsWidget = require('widgets/actionsWidget'),
			ListWidget = require('widgets/listWidget'),
			SummaryWidget = require('widgets/summaryWidget');

		var subscriptionToken = null,
			booksCollection = [];

		function addBook(book, done) {
			$.ajax({
				url: '/addBook',
				method: 'post',
				dataType: 'json',
				contentType: 'application/json',
				data: (typeof book === 'string') ? book : JSON.stringify(book),
				success: function(bookId) {
					book.setId(bookId);
					booksCollection.push(book);
					done();
				},
				error: function() {
					console.error("There was a problem adding your book");
				}
			});
			
		}

		function init(el, books) {
			var listEl = el.querySelectorAll("#list")[0],
				summaryEl = el.querySelectorAll("#summary")[0],
				actionsEl = el.querySelectorAll("#actions")[0],
				actionsWidget,
				listWidget,
				summaryWidget;

			books.forEach(function(book) {
				booksCollection.push(book);
			});

			actionsWidget = new ActionsWidget(actionsEl);
			listWidget = new ListWidget(listEl);
			summaryWidget = new SummaryWidget(summaryEl);

			subscriptionToken = pubsub.subscribe("newBookAdded",
				function(book) {
					addBook(book, function() {
						listWidget.render(booksCollection);
						summaryWidget.render(booksCollection);
					});
				});

			pubsub.subscribe("disconnect", function() {
				pubsub.unsubscribe(subscriptionToken);
			});
		}

		return {
			start: init
		};
	});

}(window.requirejs));





