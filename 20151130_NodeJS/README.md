FrontEnd WorkShops
====

## Dependencias:

* Install [node.js](http://nodejs.org/download/)
* Install [mongoDB](https://www.mongodb.org/)
* Run Mongo -> mongod
  (if you need to connect to database from terminal, use mongo command)

## Setup and Configuration:

`$ git clone https://bitbucket.org/visualengineering/frontend-workshops`  
`$ cd <projectFolder>`  
`$ sudo npm install`  
`$ sudo bower install --allow-root`  

Go to `http://localhost:3005` with your browser.


* Run node:

	`node main.js`



## Project Structure

Los ficheros importantes del proyecto son:

```
├── bower.json                # bower.json file
├── jshint.json
├── Gruntfile.js              # Gruntfile.js
├── package.json              # package.json file
├── main.js                   # node.js server file
├── public                    
│   ├── js                    # where javascript modules should be placed
│   │   ├── app.js            # main require.js configuration file
│   │   ├── ---
│   └── css           		  # styles
├── routes
│   └── index.js              # node.js web-app routes
└── templates                 # DustJS


```


# License

It's not allowed to copy, share nor publish.

Copyright 2015 Mobile Biometrics