/*
	Application request middleware index
*/

'use strict';

module.exports = {
	auth: require('./auth')
};
