'use strict';

var mongoDB = require("mongodb"),
	mongo = mongoDB.MongoClient,
	ObjectID = mongoDB.ObjectID,
	connectionString = "mongodb://localhost:27017/test";

module.exports.library = function(req, res) {
	// if you get stuck here, check your database is on!
	mongo.connect(connectionString, function(err, db){
		var books = [],
			collection,
			stream;
		if (!err) { //We are connected
			
			collection = db.collection('books');
			stream = collection.find().stream();
			
			stream.on("data", function (book) {
				books.push(book);
			});
			
			stream.on("end", function () {
				//fired when there is no more document to look for
				res.render('library', {
					books: books
				});
			});
		}
	});
};

module.exports.addBook = function(req, res, next) {
	mongo.connect(connectionString, function(err, db){
		var collection,
			book;
		if (!err) {
			// We are connected
			collection = db.collection('books');
			book = req.body;
			book._id = ObjectID.str;

			collection.insert(book, function(err, result){
				if (err) {
					next(err);
				} else {
					res.json({id: book._id});
				}
			});

		} else {
			next(err);
		}
	});
};