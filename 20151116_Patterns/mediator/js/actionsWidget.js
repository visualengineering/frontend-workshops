

var ActionsWidget = (function(pubsub, Book) {

	'use strict';

	function ActionsWidget(el) {
		var formEl,
			disconnectEl;
		this.mainEl = el;

		formEl = this.mainEl.querySelectorAll("form")[0];
		disconnectEl = this.mainEl.querySelectorAll("#disconnect")[0];

		formEl.onsubmit = this.addBook;
		disconnectEl.onclick = this.disconnect;
	}

	ActionsWidget.prototype.addBook = function(ev) {

		var form,
			book;

		ev.preventDefault();
		form = ev.target;

		book = new Book(form.elements["author"].value,
			form.elements["year"].value, form.elements["title"].value);

		pubsub.publish("newBookAdded", book);

	};

	ActionsWidget.prototype.disconnect = function(ev) {

		ev.preventDefault();

		pubsub.publish("disconnect");

	};

	return ActionsWidget;
})(window.pubsub, window.Book);



