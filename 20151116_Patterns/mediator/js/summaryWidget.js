

var SummaryWidget = (function() {

	'use strict';

	function SummaryWidget(el) {
		this.mainEl = el;
	}

	SummaryWidget.prototype.render = function(books) {

		this.mainEl.innerHTML = "There are " +
			books.length +" books in the library";

	};

	return SummaryWidget;
})();



