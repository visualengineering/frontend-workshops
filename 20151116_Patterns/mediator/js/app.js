
var App = (function(pubsub, ActionsWidget, ListWidget, SummaryWidget) {
	'use strict';

	var _private = {
		subscriptionToken: null,
		booksCollection: [],

		addBook: function(book) {
			this.booksCollection.push(book);
		},

		init: function(el, books) {
			var listEl = el.querySelectorAll("#list")[0],
				summaryEl = el.querySelectorAll("#summary")[0],
				actionsEl = el.querySelectorAll("#actions")[0],
				actionsWidget,
				listWidget,
				summaryWidget;

			books.forEach(function(book) {
				_private.addBook(book);
			});

			actionsWidget = new ActionsWidget(actionsEl);
			listWidget = new ListWidget(listEl);
			summaryWidget = new SummaryWidget(summaryEl);

			_private.subscriptionToken = pubsub.subscribe("newBookAdded",
				function(book) {
					_private.addBook(book);
					listWidget.render(_private.booksCollection);
					summaryWidget.render(_private.booksCollection);

				});

			pubsub.subscribe("disconnect", function() {
				pubsub.unsubscribe(_private.subscriptionToken);
			});
		}
	};

	return {
		start: _private.init
	};
})(window.pubsub, window.ActionsWidget, window.ListWidget, window.SummaryWidget);





