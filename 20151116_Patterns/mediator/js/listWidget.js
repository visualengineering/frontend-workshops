

var ListWidget = (function(pubsub) {

	'use strict';

	function ListWidget(el) {
		this.mainEl = el;
	}

	ListWidget.prototype.render = function(books) {
		var self = this;
		this.mainEl.innerHTML = '';
		books.forEach(function(book) {
			var el = document.createElement("li");
			el.innerHTML = book.bookInfo();
			self.mainEl.appendChild(el);
		});
	};

	return ListWidget;
})(window.pubsub);