
(function() {
	'use strict';

	requirejs.config({
		baseUrl: 'js/',
		paths: {
			pubsub: 'lib/pubsub'
		}
	});

	define(function(require) {
		var pubsub = require('pubsub'),
			ActionsWidget = require('widgets/actionsWidget'),
			ListWidget = require('widgets/listWidget'),
			SummaryWidget = require('widgets/summaryWidget');

		var subscriptionToken = null,
			booksCollection = [];

		function addBook(book) {
			booksCollection.push(book);
		}

		function init(el, books) {
			var listEl = el.querySelectorAll("#list")[0],
				summaryEl = el.querySelectorAll("#summary")[0],
				actionsEl = el.querySelectorAll("#actions")[0],
				actionsWidget,
				listWidget,
				summaryWidget;

			books.forEach(function(book) {
				addBook(book);
			});

			actionsWidget = new ActionsWidget(actionsEl);
			listWidget = new ListWidget(listEl);
			summaryWidget = new SummaryWidget(summaryEl);

			subscriptionToken = pubsub.subscribe("newBookAdded",
				function(book) {
					addBook(book);
					listWidget.render(booksCollection);
					summaryWidget.render(booksCollection);
				});

			pubsub.subscribe("disconnect", function() {
				pubsub.unsubscribe(subscriptionToken);
			});
		}

		return {
			start: init
		};
	});

}());





