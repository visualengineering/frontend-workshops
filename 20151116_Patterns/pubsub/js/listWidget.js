

var ListWidget = (function(pubsub) {

	'use strict';

	function ListWidget(el) {
		this.mainEl = el;
		this.bind();
	}

	ListWidget.prototype.render = function(books) {
		var self = this;
		this.mainEl.innerHTML = '';
		books.forEach(function(book) {
			var el = document.createElement("li");
			el.innerHTML = book.bookInfo();
			self.mainEl.appendChild(el);
		});
	};

	ListWidget.prototype.bind = function() {
		this.subscriptionToken = pubsub.subscribe("newBookAdded", this.render.bind(this));
	};

	ListWidget.prototype.unbind = function() {
		pubsub.unsubscribe(this.subscriptionToken);
	};

	return ListWidget;
})(window.pubsub);