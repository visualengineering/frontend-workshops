

var SummaryWidget = (function() {

	'use strict';

	function SummaryWidget(el) {
		this.mainEl = el;
		this.bind();
	}

	SummaryWidget.prototype.render = function(books) {

		this.mainEl.innerHTML = "There are " +
			books.length +" books in the library";

	};

	SummaryWidget.prototype.bind = function() {
		this.subscriptionToken = pubsub.subscribe("newBookAdded", this.render.bind(this));
	};

	SummaryWidget.prototype.unbind = function() {
		pubsub.unsubscribe(this.subscriptionToken);
	};

	return SummaryWidget;
})();



