
var Library = (function(pubsub, Book, ListWidget, SummaryWidget) {

	'use strict';

	var _private = {

		mainEl: null,

		booksCollection: [],

		addBook: function(book) {
			this.booksCollection.push(book);

			pubsub.publish("newBookAdded", this.booksCollection);
		},

		init: function(el, books) {
			var listEl = el.querySelectorAll("#list")[0],
				summaryEl = el.querySelectorAll("#summary")[0],
				formEl = el.querySelectorAll("form")[0],
				listWidget,
				summaryWidget;

			books.forEach(function(book) {
				_private.addBook(book);
			});

			listWidget = new ListWidget(listEl);
			summaryWidget = new SummaryWidget(summaryEl);


			formEl.onsubmit = function(ev) {
				var form,
					formData;

				ev.preventDefault();
				form = ev.target;
				_private.addBook(new Book(form.elements["author"].value,
					form.elements["year"].value, form.elements["title"].value));
			};
		}
	};

	return {
		setup: _private.init
	};


})(window.pubsub, window.Book, window.ListWidget, window.SummaryWidget);





