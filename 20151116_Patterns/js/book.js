var Book = (function() {

	'use strict';
	function Book( author, year, title ) {
		this.author = author;
		this.year = year;
		this.title = title;
	}

	Book.prototype.bookInfo = function() {
		return this.title + ", written by " + this.author + " on " + this.year;
	};

	return Book;
})();