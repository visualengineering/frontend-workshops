
var Library = (function(Subject, Observer, Book, ListWidget, SummaryWidget) {

	'use strict';

	function extend(obj, extension) {
		for(var key in extension) {
			obj[key] = extension[key];
		}
	}

	var _private = {

		mainEl: null,

		booksCollection: [],

		addBook: function(book) {
			this.booksCollection.push(book);
			if (this.notify) {
				this.notify(this.booksCollection);
			}
		},

		init: function(el, books) {
			var listEl = el.querySelectorAll("#list")[0],
				summaryEl = el.querySelectorAll("#summary")[0],
				formEl = el.querySelectorAll("form")[0];

			var listWidget = new ListWidget(listEl),
				summaryWidget = new SummaryWidget(summaryEl);

			books.forEach(function(book) {
				_private.addBook(book);
			});

			formEl.onsubmit = function(ev) {
				var form,
					formData;

				ev.preventDefault();

				form = ev.target;

				_private.addBook(new Book(form.elements["author"].value,
					form.elements["year"].value, form.elements["title"].value));
			};

			// Extend this module to be an Observable Subject  -> The Concrete Subject
			extend(_private, new Subject());

			// Extend both widget to be Observers -> The concrete observers
			extend(listWidget, new Observer());
			extend(summaryWidget, new Observer());

			listWidget.update = listWidget.render;
			summaryWidget.update = summaryWidget.render;

			// Add the widgets to the observersList
			_private.addObserver(listWidget);
			_private.addObserver(summaryWidget);
		}
	};

	return {
		setup: _private.init
	};


})(window.Subject, window.Observer, window.Book,
	window.ListWidget, window.SummaryWidget);





