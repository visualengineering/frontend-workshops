

var Observer = (function() {
	'use strict';

	function Observer() {
		this.update = function() {};
	}

	return Observer;
})();